package com.food2fork.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RecipeOld {

    @SerializedName("publisher")
    @Expose
    var publisher: String? = null
    @SerializedName("social_rank")
    @Expose
    var socialRank: Double? = null
    @SerializedName("f2f_url")
    @Expose
    var f2fUrl: String? = null
    @SerializedName("publisher_url")
    @Expose
    var publisherUrl: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("image_url")
    @Expose
    var image_url: String? = null
    @SerializedName("source_url")
    @Expose
    var sourceUrl: String? = null
    @SerializedName("page")
    @Expose
    var page: Int? = null
}

