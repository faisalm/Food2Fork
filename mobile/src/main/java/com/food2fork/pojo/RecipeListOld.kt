package com.food2fork.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RecipeListOld {

    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("recipes")
    @Expose
    var recipes: List<Recipe>? = null

}