package com.food2fork.pojo

data class RecipeList(
        val count: Int,
        val recipes: List<Recipe>
)