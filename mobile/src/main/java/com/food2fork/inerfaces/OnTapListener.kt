package com.food2fork.inerfaces

import android.content.Intent

interface OnTapListener {

    /**
     * Method intended to listen user tap if implemented. Intended to communicate back to Host (HomeActivity)
     */
    fun onTap(intent: Intent)
}