package com.food2fork.inerfaces

import com.food2fork.pojo.RecipeList

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Food2ForkEndPoints {

    @POST("search")
    @FormUrlEncoded
    fun search(@Field("key") key: String, @Field("q") q: String): Call<RecipeList>
}