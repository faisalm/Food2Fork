package com.food2fork

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecipeChangeObserver(private val recyclerView: RecyclerView, private val emptyView: TextView?) : androidx.recyclerview.widget.RecyclerView.AdapterDataObserver() {

    private val TAG = javaClass.getSimpleName()

    init {
        checkIfEmpty()
    }

    private fun checkIfEmpty() {
        if (emptyView != null && recyclerView.adapter != null) {
            val emptyViewVisible = recyclerView.adapter!!.itemCount == 0
            emptyView.visibility = if (emptyViewVisible) View.VISIBLE else View.GONE
            recyclerView.visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
        }
    }

    override fun onChanged() {
        checkIfEmpty()
    }

    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
        checkIfEmpty()
    }

    override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
        checkIfEmpty()
    }
}
