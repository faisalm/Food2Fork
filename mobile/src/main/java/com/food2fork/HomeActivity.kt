package com.food2fork

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.food2fork.inerfaces.Food2ForkEndPoints
import com.food2fork.inerfaces.OnTapListener
import com.food2fork.pojo.Recipe
import com.food2fork.pojo.RecipeList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*

class HomeActivity : AppCompatActivity(), TextView.OnEditorActionListener, Callback<RecipeList>, OnTapListener, AdapterView.OnItemClickListener {

    private var mRecyclerView: RecyclerView? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var mRecyclerAdapter: RecipeListAdapter? = null
    var recipes: List<Recipe>? = null

    private var emptyTokenListView: TextView? = null
    private var searchQuery: AutoCompleteTextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Sets the Toolbar to act as the ActionBar for this Activity.
        prepareToolbar()

        // EditText for Search query
        searchQuery = findViewById(R.id.tvSearch)
        // Get the string array
        val suggestions = resources.getStringArray(R.array.search_suggestions)
        // Create the adapter and set it to the AutoCompleteTextView
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, suggestions)
        searchQuery!!.setAdapter(adapter)
        searchQuery!!.onItemClickListener = this
        searchQuery!!.setOnEditorActionListener(this)

        // Empty tokenlist view
        emptyTokenListView = findViewById(R.id.empty_recipelist_view)

        // TokenList View
        mRecyclerView = findViewById(R.id.recyclerViewRecipeList)
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView!!.layoutManager = mLayoutManager
        mRecyclerAdapter = RecipeListAdapter(this, recipes)
        mRecyclerView!!.adapter = mRecyclerAdapter
        val recipeChangeObserver = RecipeChangeObserver(mRecyclerView!!, emptyTokenListView)
        mRecyclerAdapter!!.registerAdapterDataObserver(recipeChangeObserver)
    }

    private fun prepareToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        // Get access to the custom tvTitle view
        val tvTitle = toolbar.findViewById<TextView>(R.id.toolbar_title)
        tvTitle.visibility = View.GONE
    }

    private fun retrieveRecipes(receipeClue: String) {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val food2ForkEndPoints = retrofit.create<Food2ForkEndPoints>(Food2ForkEndPoints::class.java)
        val call = food2ForkEndPoints.search(API_KEY, receipeClue)
        call.enqueue(this)
    }

    override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE
                || event.keyCode == KeyEvent.KEYCODE_ENTER
                && event.action == KeyEvent.ACTION_DOWN) {

            // Hide keyboard
            hideKeyBoard()

            // initiate fetch
            retrieveRecipes(searchQuery!!.text.toString())
            return true
        } else {
            return false
        }
    }

    /**
     * method hides soft keyboard
     */
    private fun hideKeyBoard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    override fun onResponse(call: Call<RecipeList>, response: Response<RecipeList>) {
        println(response.body())
        val recipeList = response.body()
        recipes = Objects.requireNonNull<RecipeList>(recipeList).recipes

        // update the adapter
        mRecyclerAdapter!!.updateData(recipes)
        mRecyclerView!!.adapter = mRecyclerAdapter
        mRecyclerAdapter!!.notifyDataSetChanged()
    }

    override fun onFailure(call: Call<RecipeList>, t: Throwable) {
        t.printStackTrace()
        // Set the error text
        emptyTokenListView!!.text = "Oops! something went wrong. \nSearch again."
        // update the adapter
        mRecyclerAdapter!!.updateData(null)
        mRecyclerView!!.adapter = mRecyclerAdapter
        mRecyclerAdapter!!.notifyDataSetChanged()
    }

    override fun onTap(intent: Intent) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, findViewById(R.id.imgRecipe), "profile")
        startActivity(intent, options.toBundle())
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        // Hide keyboard
        hideKeyBoard()

        // initiate fetch
        retrieveRecipes(searchQuery!!.text.toString())
    }

    companion object {
        val BASE_URL = "http://food2fork.com/api/"
        val API_KEY = "b549c4c96152e677eb90de4604ca61a2"
    }
}
