package com.food2fork

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.food2fork.inerfaces.OnTapListener
import com.food2fork.pojo.Recipe
import com.squareup.picasso.Picasso

class RecipeListAdapter internal constructor(private val mContext: Context, private var mRecipe: List<Recipe>?) : RecyclerView.Adapter<RecipeListHolder>() {

    private val TAG = javaClass.getSimpleName()
    private val tapListener: OnTapListener

    init {
        tapListener = mContext as OnTapListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recipe_list_row, parent, false)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setElevation(40f);
        }*/
        return RecipeListHolder(view)
    }

    override fun onBindViewHolder(holder: RecipeListHolder, position: Int) {
        // Recipe image
        Log.d(TAG, "onBindViewHolder() called with: mRecipe.get(position).getImage_url() = [" + mRecipe!![position].image_url + "]")
        Picasso.get().load(mRecipe!![position].image_url)
                .fit().centerCrop()
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_image_black_24dp)
                .into(holder.imgRecipe)
        // Recipe name
        val recipeName = mRecipe!![position].title!!.replace("&amp;".toRegex(), "&").replace("&nbsp;".toRegex(), " ")
        holder.tvRecipeName.setText(recipeName)
        // Action on Detailed view
        holder.imgDetailsView.setOnClickListener { v ->
            Log.d(TAG, "onClick() called with: v = [$v]")
            val detailedIntent = Intent(mContext, DetailedActivity::class.java)
            detailedIntent.putExtra("title", recipeName)
            detailedIntent.putExtra("image_url", mRecipe!![position].image_url)
            detailedIntent.putExtra("publisher_url", mRecipe!![position].publisher)
            detailedIntent.putExtra("social_rank", mRecipe!![position].social_rank)
            detailedIntent.putExtra("source_url", mRecipe!![position].source_url)
            detailedIntent.putExtra("f2f_url", mRecipe!![position].f2f_url)
            tapListener.onTap(detailedIntent)
        }
    }

    override fun getItemCount(): Int {
        return if (mRecipe != null) mRecipe!!.size else 0
    }

    fun updateData(recipes: List<Recipe>?): List<Recipe>? {
        if (mRecipe === recipes) {
            return null
        }
        mRecipe = recipes
        if (mRecipe != null) this.notifyDataSetChanged()

        return mRecipe
    }
}