package com.food2fork

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.squareup.picasso.Picasso

import java.text.DecimalFormat

class DetailedActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = javaClass.getSimpleName()

    private var imgRecipe: ImageView? = null
    private var mImageUrl: String? = null
    private var mTitle: String? = null
    private var mPublisher: String? = null
    private var mF2FUrl: String? = null
    private var mSourceUrl: String? = null
    private var mRank: Double = 0.toDouble()
    private var tvTitle: TextView? = null
    private var tvInstruction: TextView? = null
    private var tvOriginal: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)

        // Sets the Toolbar to act as the ActionBar for this Activity.
        prepareToolbar()

        // Get intent, action and MIME type
        fetchDataFromIntent()

        // Initialize view
        initView()
    }

    private fun fetchDataFromIntent() {
        val intent = intent
        mImageUrl = intent.getStringExtra("image_url")
        mTitle = intent.getStringExtra("title")
        mPublisher = intent.getStringExtra("publisher_url")
        mRank = intent.getDoubleExtra("social_rank", 0.0)
        mF2FUrl = intent.getStringExtra("f2f_url")
        mSourceUrl = intent.getStringExtra("source_url")
    }

    private fun prepareToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        // Get access to the custom tvTitle view
        tvTitle = toolbar.findViewById(R.id.toolbar_title)
        // Remove default tvTitle text
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        // Display icon in the toolbar
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initView() {
        Log.d(TAG, "initView() called")

        // Image
        imgRecipe = findViewById(R.id.imgRecipe)
        Log.d(TAG, "initView - url : " + mImageUrl!!)
        Picasso.get().load(mImageUrl)
                .fit().centerCrop()
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_image_black_24dp)
                .into(imgRecipe)

        // Set Title
        tvTitle!!.text = mTitle

        //
        Log.d(TAG, "initView - F2F Url    : " + mF2FUrl!!)
        Log.d(TAG, "initView - Source Url : " + mSourceUrl!!)
        tvInstruction = findViewById(R.id.tvInstruction)
        tvInstruction!!.setOnClickListener(this)
        tvOriginal = findViewById(R.id.tvOriginal)
        tvOriginal!!.setOnClickListener(this)

        // Bottom views
        Log.d(TAG, "initView - Publisher : " + mPublisher!!)
        val tvPublisher = findViewById<TextView>(R.id.tvPublisher)
        tvPublisher.text = mPublisher
        Log.d(TAG, "initView - Rank : $mRank")
        val tvRank = findViewById<TextView>(R.id.tvRank)
        tvRank.text = getString(R.string.rank_text) + " : " + RoundTo2Decimals(mRank)
    }

    internal fun RoundTo2Decimals(`val`: Double): Double {
        val df2 = DecimalFormat("###.##")
        return java.lang.Double.valueOf(df2.format(`val`))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View) {
        Log.d(TAG, "onClick() called with: v = [$v]")
        when (v.id) {
            R.id.tvInstruction -> {
                Log.d(TAG, "onClick: tvIngredients" + mF2FUrl!!)
                callUrl(mF2FUrl)
            }
            R.id.tvOriginal -> {
                Log.d(TAG, "onClick: tvOriginal" + mSourceUrl!!)
                callUrl(mSourceUrl)
            }
        }
    }

    private fun callUrl(url: String?) {
        Log.d(TAG, "callUrl() called with: url = [$url]")
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
