package com.food2fork

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecipeListHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val TAG = javaClass.simpleName

    lateinit var tvRecipeName: TextView
    lateinit var imgRecipe: ImageView
    lateinit var imgDetailsView: ImageView

    init {
        // Init Views
        initViews(view)
    }

    /**
     * @param view
     */
    private fun initViews(view: View) {
        Log.d(TAG, "initViews() called with: view = [$view]")
        tvRecipeName = view.findViewById(R.id.tvRecipeName)
        imgRecipe = view.findViewById(R.id.imgRecipe)
        imgDetailsView = view.findViewById(R.id.imgDetailView)
        view.setOnClickListener { Log.d(TAG, "onClick() called with position = [$adapterPosition]") }
    }
}
